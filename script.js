var commands = [];
var width = 1000;
var radius = 20;
var u = 2 * Math.PI * radius;
var step =  ((2.0 * Math.PI * 7.5) / 4096.0) * 8;
var l;
var r;
var cpos = 3;
var drawing = true;
var points = [];
var interval;

//Reads the file which got selected by the input field and parses it by each line break into the commands array
function readSingleFile(evt) {
var f = evt.target.files[0]; 

if (f) {
	var ff = new FileReader();
	ff.onload = function(e) { 
		var contents = e.target.result;
		commands = contents.split('\n');
		width = parseInt(commands[0]);
		l = parseInt(commands[1]);
		r = parseInt(commands[2]);
		ctx.clearRect (0, 0, c.width, c.height);
		drawStage();
	}
	ff.readAsText(f);
	} else { 
		alert("Failed to load file");
	}
}

//Every time the file in the input field got changed, the file will be parsed again
$("#file").change(readSingleFile);
var c = document.getElementById("canvas");
var ctx = c.getContext("2d");
drawStage();

//draws the frame, not necessary
function drawStage()
{
	ctx.strokeRect(100,45,width,940);
	//draw circle
	ctx.beginPath();
	ctx.arc(60,25,radius,0,2*Math.PI);
	ctx.stroke();
	//draw nail
	ctx.beginPath();
	ctx.arc(99,6,1,0,2*Math.PI);
	ctx.fill();
	ctx.stroke();
	//connect
	ctx.beginPath(); 
	ctx.moveTo(60, 5);
	ctx.lineTo(98, 6);
	ctx.stroke();
	
	//draw circle
	ctx.beginPath();
	ctx.arc(width + 140,25,radius,0,2*Math.PI);
	ctx.stroke();
	//draw nail
	ctx.beginPath();
	ctx.arc(width + 100,6,1,0,2*Math.PI);
	ctx.fill();
	ctx.stroke();
	//connect
	ctx.beginPath(); 
	ctx.moveTo(width + 140, 5);
	ctx.lineTo(width + 100 ,6);
	ctx.stroke();
}

//draws every point in the points array
function drawPoints()
{
	for(var i = 0; i < points.length; i++)
		ctx.strokeRect(points[i].x + 100, points[i].y + 50,0.1,0.1);
}

//draws one step (command) each time
function drawstep(move, fast)
{
	if(!fast)
	{
		console.log("step");
		ctx.clearRect ( 0 , 0 , c.width, c.height );
		drawStage();
		drawPoints();
	}
	
	if(move == undefined && cpos > commands.length)
	{
		clearInterval(interval);
		cpos = 0;
	}
	if(move != undefined)
		tmp = move;
	else
		tmp = parseInt(commands[cpos]);
	//0 - 1.motor ab
	//1 - 1.motor auf
	//2 - 2.motor ab
	//3 - 2.motor auf
	//4 - switch druck
	switch(tmp)
	{
		case 0:
			l += step;
			break;
		case 1:
			l -= step;
			break;
		case 2:
			r += step;
			break;
		case 3:
			r-= step;
			break;
		case 4:
			drawing = !drawing;
			break;
	}
	if(!fast)
		drawCross(calcPoint(l,r), drawing);
	cpos++;
	if(drawing){
		var tmp = calcPoint(l,r);
		if(fast)
			ctx.strokeRect(tmp.x + 102, tmp.y + 50,0.1,0.1);
		points.push(tmp);
	}
}

//calculates the x and y coordinates by the two given strings
function calcPoint(l,r)
{
	w = width/2 + (l*l - r*r)/(2*width);
	h = Math.sqrt( l*l - w*w) - 44;
	return {x: w, y: h};
}

//draws the drawing head
function drawCross(point, draw)
{
	y= 50 + point.y;
	x= 100 + point.x;
	draw = typeof draw !== 'undefined' ? draw : false;
	ctx.beginPath(); 
	ctx.moveTo(x, y);
	ctx.lineTo(1100 ,6);
	ctx.stroke(); // Draw it
	
	ctx.beginPath(); 
	ctx.moveTo(x, y);
	ctx.lineTo(100, 6);
	ctx.stroke(); // Draw it

	ctx.strokeStyle="red";
	ctx.beginPath(); 
	ctx.moveTo(x - 10 , y);
	ctx.lineTo(x + 10,y);
	ctx.stroke(); // Draw it
	
	ctx.beginPath(); 
	ctx.moveTo(x, y - 10);
	ctx.lineTo(x,y + 10);
	ctx.stroke(); // Draw it
	ctx.strokeStyle="black";
	
	if(draw)
	{
		ctx.beginPath();
		ctx.arc(x,y,3,0,2*Math.PI);
		ctx.fill();
		ctx.stroke();
	}
}

//calculates the distance between two points
function lengthFromTwoPoints(pointa, pointb)
{
	var x = (pointa.x - pointb.x);
	var y = (pointa.y - pointb.y);
	return Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
}

//calculates the distance between the point (x0, x1) and the line from (x1,y1) to (x2, y2)
function dotLineLength(x0,y0,x1,y1,x2,y2) {
	var a = Math.abs((y2-y1)*x0-(x2-x1)*y0+x2*y1-y2*x1);	
	var b = Math.sqrt((y2-y1)*(y2-y1) + (x2-x1)*(x2-x1));
	console.log(a/b);	
	return a/b;	
}

//draw commands without animation
function fastPlay()
{
	for(var i = 3; i < commands.length; i++)
		drawstep(undefined, true);
}